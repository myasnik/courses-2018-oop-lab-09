package it.unibo.oop.lab.workers02;

/**
 * javadoc.
 */
public class MultiThreadedSumMatrix implements SumMatrix {

    private int numThreads;
    private Worker[] w;

    /**
     * javadoc.
     * @param n
     *  numThreads
     */
    public MultiThreadedSumMatrix(final int n) {
        this.numThreads = n;
        this.w = new Worker[numThreads];
    }

    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startpos;
        private final int nelem;
        private long res;

        /**
         * Build a new worker.
         * 
         * @param list
         *            the list to sum
         * @param startpos
         *            the initial position for this worker
         * @param nelem
         *            the no. of elems to sum up for this worker
         */
        Worker(final double[][] matrix, final int startpos, final int nelem) {
            super();
            this.matrix = matrix;
            this.startpos = startpos;
            this.nelem = nelem;
        }

        @Override
        public void run() {
            System.out.println("Working from position " + startpos + " to position " + (startpos + nelem - 1));
            for (int i = startpos; i < matrix.length && i < startpos + nelem; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    this.res += this.matrix[i][j];
                }
            }
        }

        /**
         * Returns the risult of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public long getResult() {
            return this.res;
        }

    }

    @Override
    public final double sum(final double[][] matrix) {
        int i;
        int operationsPerThread = matrix.length / this.numThreads;
        int resto = matrix.length % this.numThreads;
        int startpos = 0;
        for (i = 0; i < numThreads - 1; i++) {
            this.w[i] = new Worker(matrix, startpos, operationsPerThread);
            startpos = startpos + operationsPerThread;
        }
        this.w[i] = new Worker(matrix, startpos, operationsPerThread + resto);
        for (final Worker wo: w) {
            wo.start();
        }
        /*
         * Wait for every one of them to finish. This operation is _way_ better done by
         * using barriers and latches, and the whole operation would be better done with
         * futures.
         */
        long sum = 0;
        for (final Worker wo: w) {
            try {
                wo.join();
                sum += wo.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
        return sum;
    }

}
